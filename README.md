# easy-rotas-pos

### Pos_key
pos_key é o nome/identificação único dado a cada pos

## Autenticação

Método: POST<br>
Url: urlexemplo/auth/:pos_key<br>

Exemplo:

```
localhost:urlexemplo/auth/teste-pos-key

```

Callback:

```
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NzU0NTM3MTcsImV4cCI6MTY3NjA1ODUxNywic3ViIjoiMTIifQ.Y3Ts5u7eEfeKIfBkj78ObBEs70dldkbURm0oIBvRYwU"
}
```

## Buscar eventos

Método: GET<br>
Url: urlexemplo/events<br>
Headers: { Authorization: "Bearer token" }

Callback de exemplo:

```
[
    {
        "id": 1,
        "name": "Teste integração com a pos",
        "due_date": "2023-09-28T03:00:00.000Z", //data e hora do evento
        "age_classification": 18,
        "banner": "https://urldaimagemdoevento.jpeg",
        "address": {
            "name": "local x",
            "image": "https://urldoendereço.jpeg", //pode vim vazio
            "street": "Avenida Ivo Guersoni",
            "number": "124",
            "complement": "1234",
            "district": "Vila Beatriz",
            "city": "Pouso Alegre",
            "uf": "MG",
            "zipcode": "37555600",
            "lat": "3233323",
            "lon": "32332323"
        }
    },
    {
        "id": 5,
        "name": "CAMAROTE AVENIDA - CARNAVAL ATRASADO DE SÃO PAULO",
        "due_date": "2023-06-20T17:00:00.000Z",
        "age_classification": 18,
        "banner": "https://urldaimagemdoevento.jpeg",
        "address": {
            "name": "100- Holiday Inn parque Anhemb",
            "image": "",
            "street": "Avenida Olavo Fontoura",
            "number": "1209",
            "complement": null,
            "district": "Parque Anhembi",
            "city": "São Paulo",
            "uf": "SP",
            "zipcode": "02012-021",
            "lat": "-23.516244",
            "lon": "-46.643008"
        }
    }
]
```

## Buscar evento

Método: GET<br>
Url:urlexemplo/events/:id<br>
Headers: { Authorization: "Bearer token" }

Callback de exemplo:

```
{
    "id": 1,
    "name": "Evento x",
    "ticket_name": "teste",
    "due_date": "2023-09-28T03:00:00.000Z",
    "alternative_date_start": "2023-02-22T03:00:00.000Z",
    "alternative_date_end": "2024-01-26T03:00:00.000Z",
    "sales_start_date": "2023-02-22T03:00:00.000Z",
    "sales_end_date": "2023-10-19T03:00:00.000Z",
    "ticket_phrase": "frase no ticket",
    "description": "lorem ipsum",
    "age_classification": 18,
    "banner": "https://api.easy.risestudio.com.br/uploads/images/1677676047943-rPWKVlTgQd9of5Tui2dP7lL8B1qVwu.jpeg",
    "about": "lorem ipsum",
    "event_site": "https://local.x.com.br",
    "info": "lorem ipsum",
    "category": {
        "id": 2,
        "name": "Festivais",
        "slug": "festivais"
    },
    "address": {
        "name": "local x",
        "image": "https://api.easy.risestudio.com.br/uploads/images/1677676047943-rPWKVlTgQd9of5Tui2dP7lL8B1qVwu.jpeg",
        "street": "Avenida Ivo Guersoni",
        "number": "124",
        "complement": "1234",
        "district": "Vila Beatriz",
        "city": "Pouso Alegre",
        "uf": "MG",
        "zipcode": "37555600",
        "lat": "3233323",
        "lon": "32332323"
    },
    "ticket_classes": [
        {
            "id": 1,
            "name": "Camarote",
            "type": "normal",
            "ticket_blocks": [
                {
                    "id": 1,
                    "name": "promocional",
                    "subtotal": 123.45,
                    "tax_value": 12.35,
                    "total": 135.8,
                    "type": "default",
                    "expires_at": null
                },
                {
                    "id": 2,
                    "name": "lote antecipado",
                    "subtotal": null,
                    "tax_value": 0,
                    "total": 0,
                    "type": "courtesy",
                    "expires_at": null
                },
                {
                    "id": 3,
                    "name": "1° lote ",
                    "subtotal": 234.56,
                    "tax_value": 23.46,
                    "total": 258.02,
                    "type": "default",
                    "expires_at": null
                },
                {
                    "id": 6,
                    "name": "lote final",
                    "subtotal": 123.45,
                    "tax_value": 12.35,
                    "total": 135.8,
                    "type": "hall",
                    "expires_at": "2023-02-23T12:52:08.000Z"
                }
            ]
        }
    ]
}
```

Callback:

```
{
  "data": {
    "id": 18,
    "subtotal": 100,
    "tax_value": 5,
    "client_cpf": "12345678901",
    "total": 105,
    "origin": "pos",
    "status": "pending",
    "reference_id": null,
    "reference_object": null,
    "items": [
      {
        "ticket_id": 31,
        "ticket": {
          "code": "WN9LN0KX61XS",
          "status": "active",
          "created_at": "2023-02-03T19:53:36.000Z",
          "data": {
            "name": null,
            "email": null,
            "phone": null,
            "cpf_rg": null
          },
          "event": {
            "name": "teste",
            "slug": "teste",
            "due_date": "2023-09-01 00:00:00",
            "image": null,
            "city": "Pouso Alegre",
            "uf": "MG"
          },
          "ticket_class": {
            "name": "Entrada",
            "slug": "entrada",
            "type": "normal"
          },
          "ticket_block": {
            "name": "1",
            "slug": "1",
            "price": 50
          }
        },
        "subtotal": 50,
        "tax_value": 2.5,
        "total": 52.5
      },
      {
        "ticket_id": 32,
        "ticket": {
          "code": "1996PQA3YRJA",
          "status": "active",
          "created_at": "2023-02-03T19:53:36.000Z",
          "data": {
            "name": null,
            "email": null,
            "phone": null,
            "cpf_rg": null
          },
          "event": {
            "name": "teste",
            "slug": "teste",
            "due_date": "2023-09-01 00:00:00",
            "image": null,
            "city": "Pouso Alegre",
            "uf": "MG"
          },
          "ticket_class": {
            "name": "Entrada",
            "slug": "entrada",
            "type": "normal"
          },
          "ticket_block": {
            "name": "1",
            "slug": "1",
            "price": 50
          }
        },
        "subtotal": 50,
        "tax_value": 2.5,
        "total": 52.5
      }
    ],
    "created_at": "2023-02-03T19:53:36.000Z",
    "updated_at": "2023-02-03T19:53:36.000Z"
  }
}
```

## Confirmar compra

Método: POST<br>
Url: urlexemplo/sales/:id<br>
Headers: { Authorization: "Bearer token" }

status:
- ok = confirma a venda
- cancel = extorna a venda
- destroy = cancela a venda


```
{
    "status": "ok" | "cancel" | "destroy",
    "reference_id": "id da transação(callback do pagseguro)",
    "reference_object": "objeto da transação(callback do pagseguro)"
}
```

Callback:

```
{
  "data": {
    "id": 18,
    "subtotal": 100,
    "tax_value": 5,
    "client_cpf": "12345678901",
    "total": 105,
    "origin": "pos",
    "status": "processed",
    "reference_id": "id da transação(callback do pagseguro)",
    "reference_object": "objeto da transação(callback do pagseguro)",
    "items": [
      {
        "ticket_id": 31,
        "ticket": {
          "code": "WN9LN0KX61XS",
          "status": "sold",
          "created_at": "2023-02-03T19:53:36.000Z",
          "data": {
            "name": null,
            "email": null,
            "phone": null,
            "cpf_rg": null
          },
          "event": {
            "name": "teste",
            "slug": "teste",
            "due_date": "2023-09-01 00:00:00",
            "image": null,
            "city": "Pouso Alegre",
            "uf": "MG"
          },
          "ticket_class": {
            "name": "Entrada",
            "slug": "entrada",
            "type": "normal"
          },
          "ticket_block": {
            "name": "1",
            "slug": "1",
            "price": 50
          }
        },
        "subtotal": 50,
        "tax_value": 2.5,
        "total": 52.5
      },
      {
        "ticket_id": 32,
        "ticket": {
          "code": "1996PQA3YRJA",
          "status": "sold",
          "created_at": "2023-02-03T19:53:36.000Z",
          "data": {
            "name": null,
            "email": null,
            "phone": null,
            "cpf_rg": null
          },
          "event": {
            "name": "teste",
            "slug": "teste",
            "due_date": "2023-09-01 00:00:00",
            "image": null,
            "city": "Pouso Alegre",
            "uf": "MG"
          },
          "ticket_class": {
            "name": "Entrada",
            "slug": "entrada",
            "type": "normal"
          },
          "ticket_block": {
            "name": "1",
            "slug": "1",
            "price": 50
          }
        },
        "subtotal": 50,
        "tax_value": 2.5,
        "total": 52.5
      }
    ],
    "created_at": "2023-02-03T19:53:36.000Z",
    "updated_at": "2023-02-03T19:55:47.000Z"
  }
}
```
Callback se cancelado ou negado

```
{
  "data": {
    "id": 42,
    "subtotal": 100,
    "tax_value": 5,
    "client_cpf": "12345678901",
    "total": 105,
    "origin": "pos",
    "status": "denied" | "canceled",
    "reference_id": "id da transação(callback do pagseguro)",
    "reference_object": "objeto da transação(callback do pagseguro)",
    "items": [],
    "created_at": "2023-02-06T12:02:28.000Z",
    "updated_at": "2023-02-06T12:07:49.000Z"
  }
}
```

## Buscar vendas

Método: GET<br>
Url: urlexemplo/sales<br>
Headers: { Authorization: "Bearer token" }

Callback de exemplo:

```
{
    "pagination": {
        "total": 73,
        "perPage": 20,
        "page": 1,
        "lastPage": 4
    },
    "data": [
        {
            "id": 1,
            "subtotal": 469.12,
            "tax_value": 46.91,
            "client_cpf": "12345678901",
            "total": 516.03,
            "payment_method": "money",
            "origin": "pos",
            "status": "processed",
            "reference_id": "id da transação(callback do pagseguro)",
            "reference_object": "objeto da transação(callback do pagseguro)",
            "items": [
                {
                    "ticket_id": 1377,
                    "ticket": null,
                    "subtotal": 234.56,
                    "tax_value": 23.46,
                    "total": 258.02
                },
                {
                    "ticket_id": 1378,
                    "ticket": null,
                    "subtotal": 234.56,
                    "tax_value": 23.46,
                    "total": 258.02
                }
            ],
            "created_at": "2023-03-03T12:25:01.000Z",
            "updated_at": "2023-03-08T09:09:39.000Z"
        },
        {
            "id": 2,
            "subtotal": 246.9,
            "tax_value": 24.69,
            "client_cpf": "12345678901",
            "total": 271.59,
            "payment_method": "credit_card",
            "origin": "pos",
            "status": "processed",
            "reference_id": "id da transação(callback do pagseguro)",
            "reference_object": "objeto da transação(callback do pagseguro)",
            "items": [
                {
                    "ticket_id": 1481,
                    "ticket": null,
                    "subtotal": 123.45,
                    "tax_value": 12.35,
                    "total": 135.8
                },
                {
                    "ticket_id": 1482,
                    "ticket": null,
                    "subtotal": 123.45,
                    "tax_value": 12.35,
                    "total": 135.8
                }
            ],
            "created_at": "2023-03-06T20:02:19.000Z",
            "updated_at": "2023-03-08T09:12:59.000Z"
        },
      ]
}
```
